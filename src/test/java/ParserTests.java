import model.Instance;
import org.junit.Test;
import util.Parser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by svozil on 4/3/14.
 */
public class ParserTests {
    private Parser parser = new Parser();

    @Test
    public void parserTest1(){
        Instance example1 = parser.parseTextInstance("instances/Example1.txt");
        assertThat("Length of Schedule",example1.getScheduleL(),is(7));
        assertThat("Number of Employees",example1.getNr_employee(),is(9));
        assertThat("Number of Shifts",example1.getNr_shifts(),is(3));
        for(int i = 0; i< 3; i++){
            for(int j = 0; j< 7; j++){
                if(i==1 && (j== 3 || j== 4 || j==5 ))
                    assertThat("Temporal Requirement Matrix length",example1.getTempreq()[i][j],is(3));
                else
                    assertThat("Temporal Requirement Matrix length",example1.getTempreq()[i][j],is(2));
            }
        }

        assertThat("Shift D name",example1.getShifts()[0].getName(),is("D"));
        assertThat("Shift D maxlength",example1.getShifts()[0].getMaxLengthOfBlocks(),is(7));
        assertThat("Shift D minlength",example1.getShifts()[0].getMinLengthOfBlocks(),is(2));

        assertThat("Shift A name",example1.getShifts()[1].getName(),is("A"));
        assertThat("Shift A maxlength",example1.getShifts()[1].getMaxLengthOfBlocks(),is(6));
        assertThat("Shift A minlength",example1.getShifts()[1].getMinLengthOfBlocks(),is(2));

        assertThat("Shift N name",example1.getShifts()[2].getName(),is("N"));
        assertThat("Shift N maxlength",example1.getShifts()[2].getMaxLengthOfBlocks(),is(4));
        assertThat("Shift N minlength",example1.getShifts()[2].getMinLengthOfBlocks(),is(2));

        assertThat("minimum length day off",example1.getMin_dayoff(),is(2));
        assertThat("maximum length day off",example1.getMax_dayoff(),is(4));


        assertThat("minimum length work days ",example1.getMin_work(),is(4));
        assertThat("maximum length works days ",example1.getMax_work(),is(7));


        //assertThat("number of not allowed shift sequences",example1.getShifts_notAllowed().size() ,is(3));

        //assertThat("ND in set",example1.getShifts_notAllowed().contains("ND"));
        //assertThat("NA in set",example1.getShifts_notAllowed().contains("NA"));
        //assertThat("AD in set",example1.getShifts_notAllowed().contains("AD"));





    }
}
