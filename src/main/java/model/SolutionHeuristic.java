package model;

/**
 * Created by svozil on 11.05.14.
 */
public class SolutionHeuristic {

    private int[][] solution;
    int workdays;
    int employees;

    int fitness;

    public SolutionHeuristic (int workdays, int employees){
        solution = new int[workdays][employees];
        this.workdays=workdays;
        this.employees = employees;
        for (int i = 0; i < workdays; i++) {
            for (int j = 0; j < employees; j++) {
                solution[i][j] = -1;
            }
        }
    }
    public SolutionHeuristic (int workdays, int employees, int[][]array){
        solution = new int[workdays][employees];
        this.workdays=workdays;
        this.employees = employees;
        for (int i = 0; i < workdays; i++) {
            for (int j = 0; j < employees; j++) {
                solution[i][j] = array[i][j];
            }
        }
    }
    public void assign_single(int pos, int wblock)
    {
        this.solution[(pos)%workdays][(pos)/workdays] = wblock;
    }

    public String toString()
    {
        String out = "";
        char nice_out[] = {'-', 'D', 'A', 'N'};

       for (int j = 0; j < employees; j++) {
           for (int i = 0; i < workdays; i++) {
                out = out + nice_out[solution[i][j] + 1];
            }
            out = out + "\n";
        }
        return out;
    }

    public int[][] getArray() {
        return solution;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }
}
