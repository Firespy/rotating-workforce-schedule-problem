package model;

/**
 * Created by svozil on 15.05.14.
 */
public class TabuElement {
    int column;
    int element1;
    int element2;
    int fitness;

    public TabuElement(int column, int element1, int element2) {
        this.column = column;
        this.element1 = element1;
        this.element2 = element2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TabuElement that = (TabuElement) o;

        if (column != that.column) return false;
        if (element1 != that.element1) return false;
        if (element2 != that.element2) return false;

        return true;
    }

    @Override
    public String toString() {
        return "TabuElement{" +
                "column=" + column +
                ", element1=" + element1 +
                ", element2=" + element2 +
                ", fitness=" + fitness +
                '}';
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getElement1() {
        return element1;
    }

    public void setElement1(int element1) {
        this.element1 = element1;
    }

    public int getElement2() {
        return element2;
    }

    public void setElement2(int element2) {
        this.element2 = element2;
    }
    public int getFitness(){
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }
}
