package model;

/**
 * Created by svozil on 4/21/14.
 */
public class Partition {
    int currentSum;
    int[] setInteger;
    int pos;

    public Partition (int size)
    {
        setInteger = new int[size];
        pos = 0;
        currentSum = 0;
    }

    public Partition(int[] setInteger, int pos){
        this.setInteger = new int[setInteger.length];
        //copy solution
        for(int i=0; i<pos; i++){
            this.setInteger[i] = setInteger[i];
        }

    }

    public Partition(Partition n, int newNumber){
        setInteger = new int[n.getSetInteger().length];
        pos = n.getPos();
        currentSum = n.getCurrentSum();

        for(int i=0; i<setInteger.length; i++){
            setInteger[i] = n.getSetInteger()[i];
        }
        setInteger[pos] = newNumber;
        pos++;
        currentSum+=newNumber;

    }

    public int getCurrentSum() {
        return currentSum;
    }

    public void setCurrentSum(int currentSum) {
        this.currentSum = currentSum;
    }

    public int[] getSetInteger() {
        return setInteger;
    }

    public void setSetInteger(int[] setInteger) {
        this.setInteger = setInteger;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

}
