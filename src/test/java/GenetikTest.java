import algorithms.GeneticAlgorithm;
import model.Instance;
import model.Solution;
import model.SolutionHeuristic;
import model.TabuElement;
import org.junit.Test;
import util.Parser;

import java.util.HashMap;
import java.util.HashSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by svozil on 14.05.14.
 */
public class GenetikTest {

    @Test
    public void parserTest1() {
        Parser parser = new Parser();
        Instance i =parser.parseTextInstance("instances/Example1.txt");
        GeneticAlgorithm ga = new GeneticAlgorithm(i);
        HashSet<SolutionHeuristic> solh = new HashSet<SolutionHeuristic>();
        SolutionHeuristic sh = new SolutionHeuristic(7,9);
        int[] d1 = {2,0,-1,2,1,0,1,-1,-1};
        int[] d2 = {2,1,0,-1,2,0,-1,-1,1};
        int[] d3 = {2,1,0,-1,2,-1,-1,0,1};
        int[] d4 = {2,1,1,0,-1,-1,2,0,1};
        int[] d5 = {-1,2,1,0,-1,0,2,1,1};
        int[] d6 = {-1,2,1,1,0,0,2,1,-1};
        int[] d7 = {0,-1,2,1,0,1,2,-1,-1};
        int[][] sol = sh.getArray(); // =e1;
        sol[0] = d1;
        sol[1] = d2;
        sol[2] = d3;
        sol[3] = d4;
        sol[4] = d5;
        sol[5] = d6;
        sol[6] = d7;
        solh.add(sh);
        ga.classify(solh);

        assertThat(sh.getFitness(), is(0));

    }
     public void equaltest(){
         TabuElement element1= new TabuElement(0,0,1);
         TabuElement el2 = new TabuElement(0,0,1);
         assertThat(element1.equals(el2), is(true));
     }
}
