package algorithms;

import model.Instance;
import model.SolutionHeuristic;
import model.TabuElement;

import java.util.*;

/**
 * Created by svozil on 15.05.14.
 */
public class TabuSearch {
    Set<TabuElement> tabulist;
    Instance instance;
    SolutionHeuristic bestSol = null;

    public SolutionHeuristic solve (Instance instance){
        this.instance = instance;
        GreedyRandom gr = new GreedyRandom();
        SolutionHeuristic curS = gr.createRandom(instance);
        tabulist = new LinkedHashSet<TabuElement>();

        boolean finish_loop = true;

        List <TabuElement> nh_sols = generateNeighbourhood(curS);
        while(finish_loop){
            finish_loop = classify(nh_sols, curS);
            if(tabulist.size() > instance.getNr_employee()*12){
                Iterator<TabuElement> it = tabulist.iterator();
                it.next();
                it.remove();
            }
//            curS = selectSolution(curS, nh_sols);
        }

        return bestSol;
    }


    private SolutionHeuristic selectSolution(List<SolutionHeuristic> nh_sols) {
        return null;
    }

    private List<TabuElement> generateNeighbourhood(SolutionHeuristic curS) {
        List<TabuElement> slh = new ArrayList<TabuElement>();
        for(int j = 0; j< instance.getNr_employee(); j++){
            for(int i = 0; i < instance.getScheduleL(); i++){
                for(int k=j+1; k< instance.getNr_employee(); k++){
                    TabuElement tabuE = new TabuElement(i,j,k); //curS.getArray()[i][j], curS.getArray()[i][k]);
                    slh.add(tabuE);
                }
            }
        }
        return slh;
    }
    //classifies a set of solutions
    public boolean classify(List<TabuElement> changes, SolutionHeuristic solh) {
        int  maxWblocks = instance.getMax_work();
        int  minWblocks = instance.getMin_work();

        int  maxDblocks = instance.getMax_dayoff();
        int  minDblocks = instance.getMin_dayoff();

        TabuElement curMove = new TabuElement(-1,-1,-1);
        curMove.setFitness(Integer.MAX_VALUE);
        for(TabuElement tbe: changes){
            List<List<Integer>> shiftset = new ArrayList<List<Integer>>();
            int fitness = 0;

            int[][] sarray = solh.getArray();

            int tmp = sarray[tbe.getColumn()][tbe.getElement2()];
            sarray[tbe.getColumn()][tbe.getElement2()] = sarray[tbe.getColumn()][tbe.getElement1()];
            sarray[tbe.getColumn()][tbe.getElement1()] = tmp;// sarray[tbe.getColumn()][tbe.getElement1()];
            int penaltyWblocks = 0;
            int penaltyDOBlocks = 0;

            int curWblockLength = 0;
            int curDayoffLength = 0;

            int firstint = sarray[0][0];
            boolean skipfirstwork=false;
            boolean skipfirstdayoff=false;

            boolean lWblock = false;
            boolean lDBlock = false;

            List<Integer> curshift= new ArrayList<Integer>();


            if(firstint==-1){
                skipfirstdayoff=true;
                lDBlock=true;
            }else{
                skipfirstwork = true;
                lWblock = true;
            }
            //handle last block
            //check for bad sequence in the last blocks
            int nrw = instance.getNr_employee();
            int nrd = instance.getScheduleL();
            if(instance.isTri_bad_shifts()&&(
                    checkthreeshiftsequences(sarray[nrd-2][nrw-1],sarray[nrd-1][nrw-1],sarray[0][0])||
                            checkthreeshiftsequences(sarray[nrd-1][nrw-1],sarray[0][0],sarray[1][0])) ){
                fitness++;

            }
            if(skipfirstdayoff==true){

                outerloop:
                for(int j=0; j< instance.getNr_employee(); j++){
                    for(int i=0; i< instance.getScheduleL(); i++){
                        if(sarray[i][j] ==-1){
                            curDayoffLength++;
                        }else{
                            break outerloop;
                        }
                    }
                }
                outerloop2:
                for(int j=instance.getNr_employee()-1; 0<=j;j--){
                    for(int i=instance.getScheduleL()-1;0<=i; i--){
                        if(sarray[i][j] ==-1){
                            curDayoffLength++;
                        }else{
                            break outerloop2;
                        }

                    }
                }

            }else {
                List<Integer> tmpshift = new ArrayList<Integer>();
                outerloop4:
                for(int j=instance.getNr_employee()-1; 0<=j;j--){
                    for(int i=instance.getScheduleL()-1;0<=i; i--){
                        if(sarray[i][j] !=-1){
                            tmpshift.add(sarray[i][j]);

//                          curshift[curWblockLength] = sarray[i][j];
                            curWblockLength++;
                        }else{
                            break outerloop4;
                        }

                    }
                }
                for(int i= tmpshift.size()-1; i>=0; i--){
                    curshift.add(tmpshift.get(i));
                }

                outerloop3:
                for(int j=0; j< instance.getNr_employee(); j++){
                    for(int i=0; i< instance.getScheduleL(); i++){
                        if(sarray[i][j] !=-1){
                            curshift.add(sarray[i][j]);
                            curWblockLength++;

                        }else{
                            break outerloop3;
                        }
                    }
                }


            }
            //handle everything but the last block
            //System.out.println();
            //System.out.println("dayoff " + curDayoffLength);
            //System.out.println("wblock " + curWblockLength);
            //System.out.print(solh);
            for(int j=0; j< instance.getNr_employee(); j++){
                for(int i=0; i< instance.getScheduleL(); i++){

                    if(skipfirstdayoff || skipfirstwork){
                        if(skipfirstwork && sarray[i][j] == -1){
                            skipfirstwork = false;
                        }else if(skipfirstwork&& sarray[i][j] != -1){
                            continue;
                        }
                        if(skipfirstdayoff && sarray[i][j] == -1){
                            continue;
                        }else if(skipfirstdayoff && sarray[i][j] != -1){
                            skipfirstdayoff=false;
                        }

                    }

                    if(sarray[i][j]==-1){
                        if(lWblock){
                            if(curWblockLength > maxWblocks )
                            {
                                penaltyWblocks += curWblockLength-maxWblocks;
                                // System.out.println("putting max penalty: "+ (curWblockLength-maxWblocks));
                            }
                            if(curWblockLength < minWblocks)
                            {
                                penaltyWblocks += minWblocks - curWblockLength;
                                //System.out.println("putting min penalty: "+ (minWblocks - curWblockLength) );
                            }
                            shiftset.add(curshift);
                            if(instance.isTri_bad_shifts()) {
                                if (i + 1 < instance.getScheduleL()) {
                                    int nextint = sarray[i + 1][j];
                                    if (nextint != -1) {
                                        //System.out.println(solh);
                                        if (checkthreeshiftsequences(curshift.get(curWblockLength - 1),
                                                sarray[i][j],
                                                nextint)== true) {
                                            fitness++;
                                        }
                                    }
                                } else if (j + 1 < instance.getNr_employee()) {
                                    int nextint = sarray[0][j + 1];
                                    if (nextint != -1) {
                                        // System.out.println(solh+" " +curshift.get(curWblockLength - 1) + " " + sarray[i][j] + " "+
                                        //       nextint );
                                        if (checkthreeshiftsequences(curshift.get(curWblockLength - 1),
                                                sarray[i][j],
                                                nextint) == true) {
                                            fitness++;
                                        }
                                    }


                                }
                            }
                            curshift = new ArrayList<Integer>();// new int[instance.getNr_employee() * instance.getScheduleL()];

                            curWblockLength=0;
                            lWblock=false;
                        }
                        curDayoffLength++;
                        lDBlock = true;

                    }
                    if(sarray[i][j]!=-1){
                        if(lDBlock){
                            if(curDayoffLength > maxDblocks){
                                penaltyDOBlocks += curDayoffLength - maxDblocks;
                            }
                            if(curDayoffLength < minDblocks){
                                penaltyDOBlocks += minDblocks - curDayoffLength;
                            }
                            curDayoffLength = 0;
                            lDBlock = false;
                        }

                        curshift.add(sarray[i][j]);
                        curWblockLength++;
                        lWblock = true;

                    }

                }


            }
            //handle final block if first and last block a different
            int lastint = sarray[instance.getScheduleL()-1][instance.getNr_employee()-1];

            if(firstint == -1 && lastint !=-1 || firstint != -1 && lastint ==-1){
                if(lWblock){
                    if(curWblockLength > maxWblocks )
                    {
                        penaltyWblocks += curWblockLength-maxWblocks;
                    }
                    if(curWblockLength < minWblocks)
                    {
                        penaltyWblocks += minWblocks - curWblockLength;
                    }
                }
                if(lDBlock){
                    if(curDayoffLength > maxDblocks){
                        penaltyDOBlocks += curDayoffLength - maxDblocks;
                        //System.out.println("putting max penalty: "+ (curDayoffLength - maxDblocks));
                    }
                    if(curDayoffLength < minDblocks){
                        penaltyDOBlocks += minDblocks - curDayoffLength;
                        //System.out.println("putting min penalty: "+ (minDblocks - curDayoffLength) );
                    }
                    if(!curshift.isEmpty())
                        shiftset.add(curshift);
                }

            }




            //System.out.println(penaltyDOBlocks+" "+penaltyWblocks);
            int sum_shifts_fitness = 0;
            for(List<Integer> shift : shiftset){
                sum_shifts_fitness+= classify_singleshift(shift);
                //      System.out.print(shift.get(i));
                //   System.out.print("\n");
            }

            fitness += penaltyDOBlocks + penaltyWblocks + sum_shifts_fitness;
            // System.out.println(fitness);
            if(fitness==0) {
                // f_solution = solh;
                System.out.println("found solution");
                return false;
            }
            tbe.setFitness(fitness);
            if((tbe.getFitness() < curMove.getFitness()) && !tabulist.contains(tbe)) {
//                System.out.println("lalala");
                curMove = tbe;
            }

            tmp = sarray[tbe.getColumn()][tbe.getElement2()];
            sarray[tbe.getColumn()][tbe.getElement2()] = sarray[tbe.getColumn()][tbe.getElement1()];
            sarray[tbe.getColumn()][tbe.getElement1()] = tmp;// sarray[tbe.getColumn()][tbe.getElement1()];
            //  System.out.println(solh.toString() + solh.getFitness());
        }
        //System.out.println(curMove);
        tabulist.add(curMove);
        int[][] sarray = solh.getArray();

        int tmp = sarray[curMove.getColumn()][curMove.getElement2()];
        sarray[curMove.getColumn()][curMove.getElement2()] = sarray[curMove.getColumn()][curMove.getElement1()];
        sarray[curMove.getColumn()][curMove.getElement1()] = tmp;// sarray[curMove.getColumn()][curMove.getElement1()];
        bestSol = solh;
        //System.out.println(curMove.getFitness());
        return true;
    }

    private int classify_singleshift(List<Integer> shift){
        int fitness=0;

        int curType=-1;
        int curTypelength=1;
        // System.out.println("new shift");
       /* for(int i=0; i<shift.size(); i++){
            System.out.print(shift.get(i));
        }
        System.out.print("\n"); */

        for(int i=0; i<shift.size(); i++){
            if(curType != shift.get(i) && curType!=-1){
                if(instance.getShifts()[curType].getMaxLengthOfBlocks() < curTypelength){
                    //   System.out.println("addin maxpenalty: " + (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks()));
                    fitness += (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks());
                }
                if(instance.getShifts()[curType].getMinLengthOfBlocks() > curTypelength){
                    //  System.out.println("addin minpenalty: " +(instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength));
                    fitness += (instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength) ;
                }


                int[] shiftcheckerarry = new int[2];
                shiftcheckerarry[0] = curType;
                shiftcheckerarry[1] = shift.get(i);

                // System.out.println("shift checker array" +shiftcheckerarry[0]+" "+shiftcheckerarry[1]);
                boolean same = true;
                for(int[]ars : instance.getTabooshifts()) {
                    for (int k = 0; k < 2; k++) {
                        if (ars[k] != shiftcheckerarry[k]) {
                            same = false;
                        }
                    }
                    if(same==true){
                        // System.out.println("shift sequence not allowed");
                        fitness++;
                    }

                    same = true;

                }
                curTypelength=1;
            }else if(curType != -1){
                curTypelength++;
            }
            curType = shift.get(i);


            //for(int i=0; i<instance.getShift_taboo_lists().length; i++) {
            //   System.out.print(instance.getShift_taboo_lists()[i]);
            // }
        }
        if(curType==-1){
            curType =  shift.get(1);
        }

        if(instance.getShifts()[curType].getMaxLengthOfBlocks() < curTypelength){
            // System.out.println("addin maxpenalty: " + (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks()));
            fitness += (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks());
        }
        if(instance.getShifts()[curType].getMinLengthOfBlocks() > curTypelength){
            // System.out.println("addin minpenalty: " +(instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength));
            fitness += (instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength) ;
        }
        //  System.out.println("Fitness: " + fitness);


        return fitness;
    }

    //todo fix it
    private boolean checkthreeshiftsequences(int lastint, Integer curint, int nextint) {
        for(int[] tb : instance.getTabooshifts()){
            if(tb.length > 2){
                if(tb[0] == lastint && tb[1] == curint && tb[2]== nextint){
                    return true;
                }
            }
        }
        return false;
    }
}
