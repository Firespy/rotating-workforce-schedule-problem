import algorithms.ConstraintProgrammingTechnique;
import algorithms.GeneticAlgorithm;
import algorithms.GreedyRandom;
import algorithms.TabuSearch;
import model.Instance;
import model.Solution;
import model.SolutionHeuristic;
import util.Parser;

import java.io.File;

/**
 * Created by svozil on 4/20/14.
 */
public class Main {

    public static void main(String args[]){
        Parser parser = new Parser();
        File instances = new File("instances");
        ConstraintProgrammingTechnique exactsolver = new ConstraintProgrammingTechnique();

        for(int i = 1;i< instances.listFiles().length+1; i++){

            File curInstance = new File("instances/Example"+i+".txt");
            Instance instance = parser.parseTextInstance(curInstance.toString());
            GreedyRandom greedyRandom = new GreedyRandom();
           // System.out.println(curInstance.toString());
//            if(curInstance.toString().equals("instances/Example18.txt")){
                System.out.println("Example"+i+".txt");
                Solution sol;
            SolutionHeuristic solh;
            //TabuSearch ts = new TabuSearch();
            GeneticAlgorithm ga = new GeneticAlgorithm();
                long startTime = System.currentTimeMillis();

               //  sol = exactsolver.solve(instance);
                 long runtime = 300000;
              //  solh = ts.solve(instance);
                 solh = ga.solve(instance, runtime); //= greedyRandom.createRandom(instance);

                long endTime = System.currentTimeMillis();
                System.out.println(solh);
                System.out.println("took " + (endTime - startTime) + " milliseconds");
 //           }
        }

    }
}
