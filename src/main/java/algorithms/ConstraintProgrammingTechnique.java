package algorithms;

import model.Instance;
import model.Partition;
import model.Solution;
import model.Shift;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

/**
 * Created by svozil on 4/20/14.
 */
public class ConstraintProgrammingTechnique {

    /**
     * Solve the problem in the four steps as
     * proposed in the paper
     * @param i
     * @return
     */
    private Instance i;
    int [] tempreqd;
    int [] tempreqdoff;
    int freedays;
    int numberofWorkBlocks;
    private HashMap<Integer, Set<int[]>> shiftSequences;
    boolean solved;
    private long starttime;

    public Solution solve(Instance i){
        //initstuff
        this.i = i;
        this.solved = false;
        shiftSequences = new HashMap<Integer, Set<int[]>>();

        tempreqd = new int[i.getScheduleL()];
        tempreqdoff = new int[i.getScheduleL()];

        for(int j = 0; j    < i.getNr_shifts(); j++){
            for(int k =0; k < i.getScheduleL(); k++){
                tempreqd[k]     = tempreqd[k] +i.getTempreq()[j][k];
                tempreqdoff[k]  = tempreqd[k] +i.getTempreq()[j][k];
            }
        }
        for(int k =0; k < i.getScheduleL(); k++){
            tempreqdoff[k]  = i.getNr_employee() - tempreqd[k];

        }

        Solution sol = new Solution(i.getScheduleL(), i.getNr_shifts());
        sol = lengthsOfWorkBlocks(sol);

        return sol;
    }


    /*
    This method is about creating so-called class solutions, where blocks are shown in decreasing order.
    A class solution is nothing but an integer partition of the sum of all working days scheduled for one employee
    during the entire planning period. (Sum of each element in requirement matrix)

    Maximum and minimum lengths of days-off blocks impact the maximum and minimum permitted number of elements
    in one partition, since between two work blocks there always is one block of days-off, or a block for recreation.

    Minimum number of elements in a partition: MINB = ceil(DaysOffSum / MAXS(m)), m = shift
    Maximum number of elements in a partition MAXB = floor(DaysOffSum/MINS(m))
     */
    private Solution lengthsOfWorkBlocks(Solution sol) {
        int daysoffsum = (i.getNr_employee()*i.getScheduleL())-sumMatrix(i.getTempreq());
        //System.out.println("Days off " + daysoffsum + " " + (i.getNr_employee()*i.getScheduleL()));
        freedays = daysoffsum;

        //minimum length of elements in a partition
        int minB =(int) Math.ceil((double) daysoffsum / (double) i.getMax_dayoff());

        //maximum length of elements in a partition
        int maxB =(int) Math.floor((double) daysoffsum / (double) i.getMin_dayoff());

        //Solve the partition problems
        return create_restrictedPartitionsOpt(sumMatrix(i.getTempreq()),i.getMin_work(), maxB,new int[maxB],0,0);
    }

    public static int[] toPrimitive(Integer[] IntegerArray) {

        int[] result = new int[IntegerArray.length];
        for (int i = 0; i < IntegerArray.length; i++) {
            result[i] = IntegerArray[i].intValue();
        }
        return result;
    }

    private int[][] createUniqueWorkBlocks(Partition p) {
        numberofWorkBlocks = 0;
        int[] partition = p.getSetInteger();

        int lastInt = partition[0];

        int numberOfUniqueWorkBlocks = 1;
        List<Integer> distinctIntegers = new ArrayList<Integer>();
        distinctIntegers.add(partition[0]);


        for(int i=0; i<partition.length; i++){
            if(lastInt != partition[i] && partition[i] != 0){
                numberOfUniqueWorkBlocks++;
                lastInt = partition[i];
                distinctIntegers.add(lastInt);
            }
        }

        int[][] uniqueWorkblocks = new int[2][numberOfUniqueWorkBlocks];
        uniqueWorkblocks[0] = toPrimitive(distinctIntegers.toArray(new Integer[numberOfUniqueWorkBlocks]));

        lastInt = partition[0];
        for(int i = 0, k=0; i<partition.length; i++){
            if(lastInt != partition[i] && partition[i] != 0){
                lastInt = partition[i];
                k++;
                uniqueWorkblocks[1][k] = 1;

                numberofWorkBlocks++;
            }else if (partition[i] != 0){
                uniqueWorkblocks[1][k] = uniqueWorkblocks[1][k]+1;

                numberofWorkBlocks++;
            }
        }
        return uniqueWorkblocks;
    }

    //creates all possible partitions concerning the work blocks
    private Solution create_restrictedPartitionsOpt(int p_number,int minB, int maxB, int[] inputPart, int pos, int inputSum)
    {
        boolean partitionComplete = false;
        int[]curPart =  inputPart;
        int cursum = inputSum;
        //the number to be added
        int counter = minB;//this.i.getMin_work();
        while(counter <= this.i.getMax_work() && !partitionComplete){
            curPart[pos] = counter;
            if(cursum+counter == p_number){
                partitionComplete = true;

                Partition p = new Partition(curPart, pos+1);
                numberofWorkBlocks =0;
                int[][] uniqueWorkBlocks = createUniqueWorkBlocks(p);

                starttime = System.currentTimeMillis();
                Solution ret = check_restrictedPartitionsOpt(1, uniqueWorkBlocks, new boolean[i.getScheduleL() * i.getNr_employee()],
                        0, new int[numberofWorkBlocks * 2], 0, starttime );

                if (ret != null) {
                    return ret;
                }
            }
            else if(cursum < p_number-i.getMin_work() && pos+1 < maxB){
                Solution ret = create_restrictedPartitionsOpt(p_number,counter, maxB, curPart, pos+1, inputSum+counter );

                if (ret != null) {
                    return ret;
                }
            }
            counter++;
        }

        return null;
    }

    /*
    Checks partitions with backtracking for a
    legal distribution of work and days-off blocks.
    if i  is odd, add a workblock, otherwhise add a day off-block
     */
    private Solution check_restrictedPartitionsOpt(int i, int[][] uniqueWorkblocks, boolean[] distribution, int distPos,
                                               int[] sequence, int sequencePos, long runningtime
                                               ){
        if (this.solved || runningtime - starttime > 400) {
            return null;
        }

        //the counter for the respective element in the  unique work block or unique day off block list
        if (i%2 == 1){
            //add work blocks
            for(int j=0; j<uniqueWorkblocks[0].length; j++ ){
                int workblock =  uniqueWorkblocks[0][j];

                if(workblock+distPos > distribution.length){
                    return null;
                }

                for(int k=distPos; k < distPos+workblock; k++){
                    distribution[k] = false;
                }

                uniqueWorkblocks[1][j]  = uniqueWorkblocks[1][j]-1;
                sequence[sequencePos] = workblock;
                if(uniqueWorkblocks[1][j]>=0 && fullfillsRequirements(tempreqd, this.i.getScheduleL(), distribution, distPos+workblock)){
                    Solution sol = check_restrictedPartitionsOpt(i + 1, uniqueWorkblocks, distribution, distPos + workblock, sequence,
                            sequencePos + 1, System.currentTimeMillis());

                    if (sol != null) {
                        this.solved = true;
                        return sol;
                    }
                }
                //redo for next recusion
                uniqueWorkblocks[1][j]  = uniqueWorkblocks[1][j]+1;

            }
        }
        else{
            //add day off block

            for (int j = 0; j < this.i.getMax_dayoff() - this.i.getMin_dayoff() + 1; j++) {

                int dayOff_block =  this.i.getMin_dayoff() + j;

                if(dayOff_block+distPos > distribution.length)
                    return null;

                for(int k=distPos; k < distPos+dayOff_block; k++){
                    distribution[k] = true;
                }

                sequence[sequencePos] = dayOff_block;

                if(i==numberofWorkBlocks*2){
                    if(sumTest(freedays, distribution)){

                        Solution sol =  sequences_shifts_assignment(sequence, 1, 0, new Solution(this.i.getScheduleL(), this.i.getNr_employee()), false);

                        if (sol != null) {
                            this.solved = true;
                            return sol;
                        }

                        return null;
                    }
                }else if(fullfillsRequirementsDayOff(tempreqdoff, this.i.getScheduleL(), distribution, distPos+dayOff_block)){

                    Solution sol = check_restrictedPartitionsOpt(i + 1, uniqueWorkblocks, distribution, distPos+dayOff_block, sequence,
                            sequencePos + 1, System.currentTimeMillis());

                    if (sol != null) {
                        this.solved = true;
                        return sol;
                    }
                }
            }
        }

        return null;
    }

    private boolean fullfillsRequirementsDayOff(int[] tempreqdayOff, int schedulelength, boolean[] distribution, int pos) {
        int [] countermatrix =new int[schedulelength];
        for(int i=0; i<pos; i++){
            if(distribution[i]==true){
                countermatrix[i%schedulelength] = countermatrix[i%schedulelength] + 1;
            }
        }

        for(int i = 0; i<schedulelength; i++){
            if (countermatrix[i] >  this.tempreqdoff[i]){
               // System.out.println("rejected");
                return false;
            }
        }
        return true;
    }

    private boolean sumTest(int freedays, boolean[] distribution) {
        int cntdayoffs = 0;
        for(int i=0; i<distribution.length; i++){
            if(distribution[i]==true){
                cntdayoffs++;
            }

        }
        if(freedays == cntdayoffs){
            //System.out.println(freedays + " " + cntdayoffs);
            return true;
        }
        return false;
    }

    private boolean fullfillsRequirements(int[] tempreqday, int schedulelength, boolean[] distribution, int pos) {
        int [] countermatrix =new int[schedulelength] ;
        for(int i=0; i<pos; i++){
            if(distribution[i]==false){
                countermatrix[i%schedulelength] = countermatrix[i%schedulelength] + 1;
            }
        }

        for(int i = 0; i<schedulelength; i++){
            if (countermatrix[i] >  tempreqday[i]){
         //       System.out.println("rejected2");
                return false;
            }
        }
        return true;
    }

    private void generate_shift_sequences(int[] s, int pos, int seq, int wblength)
    {
        int last;

        if (pos == 0) {
            last = -1;
        }
        else {
            last = s[pos - 1];
        }


        for (int i = 0; i < this.i.getShifts().length; i++) {
            if (pos == 0 || !this.i.getShift_taboo_lists()[last].contains(i)) {
                int ls[] = s.clone();
                int lpos = pos;
                int lseq = seq;

                if (lpos == 0 || last != i) {
                    //we are at the start of the shseq or adding a different shift than before
                    //add the shift minlength times
                    lseq = 0;
                    int limit = this.i.getShifts()[i].getMinLengthOfBlocks() + lpos;
                    if (limit <= wblength) {
                        for (; lpos < limit; lpos++) {
                            ls[lpos] = i;
                            lseq++;
                        }
                        if (lpos == wblength) {
                            this.shiftSequences.get(wblength).add(ls);
                        }
                        else {
                            generate_shift_sequences(ls, lpos, lseq, wblength);
                        }
                    }
                }
                else {
                    //we add a single shift of the same type as before
                    //check maxlength
                    if (lseq + 1 <= this.i.getShifts()[i].getMaxLengthOfBlocks()) {
                        ls[lpos++] = i;
                        lseq++;
                        if (lpos == wblength) {
                            this.shiftSequences.get(wblength).add(ls);
                        }
                        else {
                            generate_shift_sequences(ls, lpos, lseq, wblength);
                        }
                    }
                }
            }
        }
    }

    /**
     * Search for legal paths in a search tree. The levels of the tree represent work blocks and the
     * branches of the tree correspond to the allocation of a sequence of shifts. To prune, the procedure uses
     * the information about the needed number of employees in each shift (for each day)
     * If there exist shift changes constraints that include days-off, then test of the solution has to be done
     * later for these sequences
     *
     */
    private Solution sequences_shifts_assignment(int sequence[], int i, int pos, Solution sol, boolean check_long_seqs)
    {
        int wblength = sequence[i-1];

        //if at end
        if (i == sequence.length || wblength == 0) {
            if (sol.check_tempreqs(this.i.getTempreq(), -1)) {
                return sol;
            }
            return null;
        }

        if (i%2 == 1) {
            //add work block
            Set<int[]> sseqs = this.shiftSequences.get(wblength);

            if (sseqs == null) {
                sseqs = new HashSet<int[]>();
                this.shiftSequences.put(wblength, sseqs);

                //generate all sequences of length wblength, honoring the constraints
                generate_shift_sequences(new int[wblength], 0, 0, wblength);
            }

            //try every shift sequence with length wblength
            for (int shiftseq[] : sseqs) {
                boolean taboo = false;

                sol.assign_wblock(pos, shiftseq);

                int tmp_pos = pos + shiftseq.length;

                //check constraint
                if (check_long_seqs) {
                    List<int[]> taboo_list = this.i.getShift_long_taboo_lists();

                    for (int[] tab : taboo_list) {
                        if (sol.is_taboo(pos - 2, tab)) {
                            taboo = true;
                            break;
                        }
                    }
                }

                if (!taboo) {
                    if (sol.check_tempreqs(this.i.getTempreq(), tmp_pos)) {
                        Solution ret = sequences_shifts_assignment(sequence, i + 1, tmp_pos, sol, false);
                        if (ret != null) {
                            return ret;
                        }
                    }
                }
            }

            check_long_seqs = false;
        }
        else {
            //add days off block
            int offseq[] = new int[wblength];
            boolean long_seqs = false;

            if (wblength == 1) {
                long_seqs = true;
            }

            for (int j = 0; j < offseq.length; j++) {
                offseq[j] = -1;
            }

            sol.assign_wblock(pos, offseq);
            pos += wblength;

            Solution ret = sequences_shifts_assignment(sequence, i + 1, pos, sol, long_seqs);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    private int sumMatrix(int[][] a){
        int sum = 0;
      for(int j = 0; j < this.i.getNr_shifts(); j++){
       for(int i = 0; i < this.i.getScheduleL(); i++){
              sum+= a[j][i];
           }
       }

        return sum;
    }

}

