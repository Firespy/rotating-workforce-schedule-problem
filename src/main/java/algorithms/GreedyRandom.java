package algorithms;

import model.Instance;
import model.Solution;
import model.SolutionHeuristic;

import java.util.Random;

/**
 * Created by svozil on 11.05.14.
 */

/* Creates random solution */
public class GreedyRandom {

    Random r = new Random();

    public SolutionHeuristic createRandom(Instance instance){
        //create Solution array

        //time requirement for each shift

        //System.out.println("Number of Shifts: " + instance.getNr_shifts());
        int nr_employees = instance.getNr_employee();
        int scheduleLength = instance.getScheduleL();
        int nr_shifts = instance.getNr_shifts();

        int [][] tempreqd = new int[nr_shifts][scheduleLength];

        // tiefe kopie
        for(int i = 0; i<instance.getNr_shifts(); i++){
            for(int j = 0; j<instance.getScheduleL(); j++){
               tempreqd[i][j]= instance.getTempreq()[i][j];
            }

        }


        SolutionHeuristic sol = new SolutionHeuristic(scheduleLength,nr_employees);
        int [][] solutionarray = sol.getArray();
        //go through the temporal requirement matrix
        for(int i = 0 ;i < nr_shifts; i++){
           for(int j=0;j < scheduleLength; j++ ){


              while(tempreqd[i][j] > 0){
                boolean found  = false;
                  while(!found){
                      //look for an employee without a shift randomly
                      int k = r.nextInt(nr_employees);
                      if(solutionarray[j][k] == -1) {
                          solutionarray[j][k] = i;
                          break;
                      }
                  }
                  tempreqd[i][j]  = tempreqd[i][j]-1;
              }
           }
        }

        //would display requirement matrix after randomizing
       /* for(int i = 0 ;i < nr_shifts; i++){
            for(int j=0;j < scheduleLength; j++ ) {
                System.out.print(tempreqd[i][j]);

            }
            System.out.println();

        }*/

        return sol;
    }
}
