package model;

import java.util.*;

/**
 * Created by svozil on 4/3/14.
 */
public class Instance {
    /*Length of the schedule*/
    private int scheduleL;

    /*Number of Employees*/
    private int nr_employee;

    /* Number of Shifts*/
    private int nr_shifts;

    /* Temporal Requirements Matrix */
    private int tempreq[][];

    /* Shifts with min length of blocks and maxlengthof blocks */
    /* 0: D - Day 1: A - Afternoon 2: N - Night
       Has to be an array because there are instances with arbitrary shifts
     */
    private Shift shifts[];

    /* Maximum length of days-off*/
    private int max_dayoff;

    /* Minimum length of days off*/
    private int min_dayoff;

    /* Maximum length of work blocks*/
    private int max_work;

    /* Minimum length of work blocks*/
    private  int min_work;

    public boolean isTri_bad_shifts() {
        return tri_bad_shifts;
    }

    public void setTri_bad_shifts(boolean tri_bad_shifts) {
        this.tri_bad_shifts = tri_bad_shifts;
    }

    private boolean tri_bad_shifts;
    /*Set of not  allowed shifts, i,e, {ND,NA,AD,N-N}*/
    private List<int[]> shift_long_taboo_lists;

    private HashSet<int[]> tabooshifts;

    //other representation for not allowed shift sequences of length 2
    //much nicer to work with than strings
    //not allowed sequences of length 3 contain free days, so we have to handle them at a different level
    private TabooList shift_taboo_lists[];

    //well, fuck you too Java, all I wanted was
    //private HashSet<Integer> shift_taboo_lists[];
    public class TabooList {
        private HashSet<Integer> tablist;

        public TabooList() {
            this.tablist = new HashSet<Integer>();
        }

        public boolean contains(int i) {
            return this.tablist.contains(i);
        }

        public void add(int i) {
            this.tablist.add(i);
        }
    }

    public int getScheduleL() {
        return scheduleL;
    }

    public int getNr_employee() {
        return nr_employee;
    }

    public int getNr_shifts() {
        return nr_shifts;
    }

    public int[][] getTempreq() {
        return tempreq;
    }

    public Shift[] getShifts() {
        return shifts;
    }

    public int getMax_dayoff() {
        return max_dayoff;
    }

    public int getMin_dayoff() {
        return min_dayoff;
    }

    public int getMax_work() {
        return max_work;
    }

    public int getMin_work() {
        return min_work;
    }

    public TabooList[] getShift_taboo_lists() {
        return this.shift_taboo_lists;
    }

    public List<int[]> getShift_long_taboo_lists() {
        return this.shift_long_taboo_lists;
    }

    public void setScheduleL(int scheduleL) {
        this.scheduleL = scheduleL;
    }

    public void setNr_employee(int nr_employee) {
        this.nr_employee = nr_employee;
    }

    public void setNr_shifts(int nr_shifts) {
        this.nr_shifts = nr_shifts;
    }

    public void setTempreq(int[][] tempreq) {
        this.tempreq = tempreq;
    }

    public void setShifts(Shift[] shifts) {
        this.shifts = shifts;
    }

    public void setMax_dayoff(int max_dayoff) {
        this.max_dayoff = max_dayoff;
    }

    public void setMin_dayoff(int min_dayoff) {
        this.min_dayoff = min_dayoff;
    }

    public void setMax_work(int max_work) {
        this.max_work = max_work;
    }

    public void setMin_work(int min_work) {
        this.min_work = min_work;
    }


    public void parseShift(HashSet<String> shifts_notAllowed){
        //convert to other represantation because I like it that way
        this.shift_taboo_lists = new TabooList[3];
        this.shift_taboo_lists[0] = new TabooList();
        this.shift_taboo_lists[1] = new TabooList();
        this.shift_taboo_lists[2] = new TabooList();

        char[] shifts = {'-', 'D', 'A', 'N'};

        this.shift_long_taboo_lists = new ArrayList<int[]>();

        //OMG how ugly! my eyes, my precious eyes!
        //but now I can do
        //if (!shift_taboo_lists[prev].contains(cur))
        //when generating shift sequences, so I'm happy with myself
        for (String sna : shifts_notAllowed) {
            if (sna.length() == 2) {
                for (int i = 0; i < shifts.length - 1; i++) {
                    if (sna.charAt(0) == shifts[i + 1]) {
                        for (int j = 0; j < shifts.length - 1; j++) {
                            if (sna.charAt(1) == shifts[j + 1]) {
                                this.shift_taboo_lists[i].add(j);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            else if (sna.length() > 2) {
                int[] tmp = new int[sna.length()];

                for (int i = 0; i < sna.length(); i++) {
                    for (int j = 0; j < shifts.length; j++) {
                        if (sna.charAt(i) == shifts[j]) {
                            tmp[i] = j - 1;
                        }
                    }
                }

                this.shift_long_taboo_lists.add(tmp);
            }
        }
    }

    public void setShifts_notAllowed(HashSet<String> shifts_notAllowed){
        parseShift(shifts_notAllowed);
        parseShiftReloaded(shifts_notAllowed);

    }

    private void parseShiftReloaded(HashSet<String> shifts_notAllowed) {
        tabooshifts = new HashSet<int[]>();
        ArrayList<Character> shifts = new ArrayList<Character>();// {'-', 'D', 'A', 'N'};
        shifts.add('-');
        shifts.add('D');
        shifts.add('A');
        shifts.add('N');
        tri_bad_shifts = false;

        for(String shift : shifts_notAllowed) {
            if(shift.length()>=3){
                tri_bad_shifts=true;
              //  System.out.println("trishifttype");
            }
           int[] curshift = new int[shift.length()];

            for(int i=0; i<shift.length(); i++){
                curshift[i] = shifts.indexOf(shift.charAt(i))-1;

            }
            tabooshifts.add(curshift);
            //System.out.println(curshift[0] + " "+ curshift[1]);


            //System.out.println(shift);
        }
    }

    public HashSet<int[]> getTabooshifts() {
        return tabooshifts;
    }
}
