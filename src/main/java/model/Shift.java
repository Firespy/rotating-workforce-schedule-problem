package model;

/**
 * Created by svozil on 4/3/14.
 */
public class Shift {

    /* Name of the shift e.g. A,D,N */
    private String name;

    /* Minimum length of Blocks*/
    private int minLengthOfBlocks;

    /* Maximum Length of Blocks*/
    private int maxLengthOfBlocks;

    public String getName() {
        return name;
    }

    public int getMinLengthOfBlocks() {
        return minLengthOfBlocks;
    }

    public int getMaxLengthOfBlocks() {

        return maxLengthOfBlocks;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinLengthOfBlocks(int minLengthOfBlocks) {
        this.minLengthOfBlocks = minLengthOfBlocks;
    }

    public void setMaxLengthOfBlocks(int maxLengthOfBlocks) {
        this.maxLengthOfBlocks = maxLengthOfBlocks;
    }
}
