package model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by svozil on 13.05.14.
 */
public class FitnessClass {
    private int fitness;
    private Set<SolutionHeuristic> solh;

    public FitnessClass(int fitness) {
        this.fitness = fitness;
        solh =  new HashSet<SolutionHeuristic>();
    }

    public int getFitness() {
        return fitness;
    }

    public Set<SolutionHeuristic> getSolh() {
        return solh;
    }
    public SolutionHeuristic removeSolh(){
        SolutionHeuristic w= solh.iterator().next();
        solh.remove(w);
        return  w;

    }
}
