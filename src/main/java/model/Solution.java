package model;

/**
 * Created by svozil on 4/20/14.
 */
public class Solution {
    private int[][] solution;

    public Solution (int workdays, int shifts){
        solution = new int[workdays][shifts];

        for (int i = 0; i < workdays; i++) {
            for (int j = 0; j < shifts; j++) {
                solution[i][j] = -1;
            }
        }
    }

    public String toString()
    {
        String out = "";
        char nice_out[] = {'-', 'D', 'A', 'N'};

        for (int i = 0; i < solution[0].length; i++) {
            for (int j = 0; j < solution.length; j++) {
                out = out + nice_out[ solution[j][i] + 1];
            }
            out = out + "\n";
        }
        return out;
    }

    public void assign_wblock(int pos, int wblock[])
    {
        int workdays = this.solution.length;
        int shifts = this.solution[0].length;

        for (int i = 0; i < wblock.length; i++) {
            this.solution[(pos+i)%workdays][(pos+i)/workdays] = wblock[i];
        }
    }

    //limit < 0 --> check everything
    public boolean check_tempreqs(int tempreq[][], int limit)
    {
        //tempreq[nr_shifts][scheduleL]
        int l_tempreq[][] = new int[tempreq.length][];

        for (int i = 0; i < l_tempreq.length; i++) {
            l_tempreq[i] = tempreq[i].clone();
        }

        int counter = 0;
        for (int i = 0; i < this.solution[0].length; i++) {
            for (int j = 0; j < this.solution.length; j++) {
                counter++;
                if (limit >= 0 && counter > limit) {
                    break;
                }
                int shift = this.solution[j][i];
                if (shift >= 0) {
                    l_tempreq[shift][j]--;
                }
            }
        }

        for (int i = 0; i < l_tempreq.length; i++) {
            for (int j = 0; j < l_tempreq[0].length; j++) {
                if ((limit >= 0 && l_tempreq[i][j] < 0) || (limit < 0 && l_tempreq[i][j] != 0)) {
                    return false;
                }
            }
        }

        return true;
    }

    public boolean is_taboo(int pos, int[] taboo)
    {
        boolean ret = true;
        int workdays = this.solution.length;

        for (int i = 0; i < taboo.length; i++) {
            ret = ret && this.solution[(pos+i)%workdays][(pos+i)/workdays] == taboo[i];
        }

        return ret;
    }

}
