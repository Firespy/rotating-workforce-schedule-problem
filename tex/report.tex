% Author: Alexander Svozil, Andreas Moßburger(1029147)
% Matr.-nr.: 1026213, 1029147
% TU Wien
\documentclass [12pt]{article}
\usepackage [utf8]{inputenc}
\usepackage {graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{float}
\usepackage{pgfplots}
\usepackage{setspace}
\usepackage{amsmath}
\usepackage{color}
\usepackage{xcolor}
\usepackage{listings}
\usetikzlibrary{shapes.multipart}
\usepackage{caption}
\lstset{language=Java}


\bibliographystyle{ieeetr} 
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\newtheorem{mydef}{Definition}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\begin{document}
\vspace{20pt}
\begin{center}
  \fontsize{45pt}{25pt}\selectfont Project Report 
\end{center}
\vspace{50pt}
\begin{center}
  \fontsize{55pt}{30pt}\selectfont\textbf{Rotating Workforce Schedule} 
\end{center}
\vspace{50pt}
\begin{center}
  \large Andreas Moßburger (1029147), Alexander Svozil (1026213)\\April 30, 2014
\end{center}
%\maketitle
\thispagestyle{empty}
\newpage


\section*{Abstract}
An exact solver for the rotating workforce scheduling problem was implemented
and the steps to achieve a solution are documented. 
\thispagestyle{empty}


\section{Solving the Rotating Workforce Schedule}
We were heavily inspired by the method described in \cite{Musliu200285}. We slightly modified the four steps described in the paper: (i) Determination of lengths
of work blocks (ii) Determination of legal restricted partitions (iii) Generation of permitted shift sequences for each work block 
(iv) Assignment of shift sequences to work blocks. As our goal was not to find a solution which had optimal weekend characteristics we ignored the soft constraint described in \cite{Musliu200285}
and focused on finding a solution that was valid.

\section{Determination of lengths of work blocks}
In the first step, we try to find so called 'class solutions'. As described in \cite{Musliu200285} a class solution is
nothing but an integer partition of the sum of all working days scheduled for one employee
during the entire planning period. This problem is solved using a backtracking algorithm. The backtracking algorithm is documented in Listing \ref{intpart}.
The following parameters are needed to start the function: $p\_number$ is the sum of all working days scheduled for one employee during the entire planning period.
$inputPart$ is an integer array storing the current partition. $pos$ is the position the algorithm is currently at in the integer array. $minB$ is the minimum integer the
backtracking algorithm will try to add. $maxB$ is the maximum size of the partition. $inputSum$ is the current sum.
$max\_WorkBlock$ is the length of the maximum work block (this is part of the instance). 
$min\_WorkBlock$ is the length of the minimum work block (this is part of the instance). 
The algorithm starts adding numbers from $minB$ to $max\_WorkBlock$ to the partition array $inputPart$ in the while-loop.
As long as the sum of the integers in $inputPart$ are smaller than $p\_number - min\_WorkBlock$ and $pos+1$ does not exceed $maxB$ the function is called recursively and new blocks are added to
$inputPart$. As we only want class solutions we pass the current added number as new $min\_WorkBlock$.  If a partition is found, i.e., $cursum+counter = p\_number$, step two of our algorithm starts, namely "Determination of legal restricted partitions".

\begin{lstlisting}[label=intpart,caption=Determination of lengths of workblocks, tabsize=1, keepspaces=true, breaklines=true]
   private Solution create_restrictedPartitionsOpt(int p_number, int minB, int maxB, int[] inputPart, int pos, int inputSum)
    {
        boolean partitionComplete = false;
        int[]curPart =  inputPart;
        int cursum = inputSum;
        //the number to be added
        int counter = minB;
        while(counter <= max_WorkBlock && !partitionComplete){
            curPart[pos] = counter;
            if(cursum+counter == p_number){
                partitionComplete = true;
                Partition p = new Partition(curPart, pos+1);
                numberofWorkBlocks =0;
                int[][] uniqueWorkBlocks = createUniqueWorkBlocks(p);
                starttime = System.currentTimeMillis();
                Solution ret = check_restrictedPartitionsOpt(1, uniqueWorkBlocks, new boolean[scheduleLength*number_Employees], 0, new int[numberofWorkBlocks * 2], 0, starttime );
                if (ret != null) {
                    return ret;
                }
            }
            else if(cursum < p_number-min_WorkBlock && pos+1 < maxB){
                Solution ret = create_restrictedPartitionsOpt(p_number,counter, maxB, curPart, pos+1, inputSum+counter );
                if (ret != null) {
                    return ret;
                }
            }
            counter++;
        }
        return null;
    }
\end{lstlisting}

\section{Determination of legal restricted partitions}
Some of the created restricted partitions are not legal and thus all the restricted partitions that can fulfill the work force per day requirements are created. If we find such a permutation of a restricted partition the next step is initiated.
The algorithm for this particular problem is displayed in Listing \ref{detlegal}.
The following parameters are utilized to make the backtracking algorithm work: $i$ determines whether a workblock or a  day-off block is added, $uniqueWorkblocks$ is a two dimensional array saving the workblocks left (if the restricted partition is $3 3 3 4 4 4 5$, $uniqueWorkblocks$ would look like this  
$\begin{matrix}
    3 & 4 & 5 \\
    3 & 3 & 1 
\end{matrix}$),
$distribution$ is a boolean array of size $scheduleLength \cdot nr\_employees$ which saves the current distribution of working and day-off blocks by saving  $false$ on a workblock and $true$ on a day-off block,   $distPos$
is simply the position of the backtracking algorithm in the distribution array, $sequence$ is the current distribution in an int array saving working  and day-off blocks alternatingly, $sequencePos$ is the position of the
backtracking algorithm in the $sequence$ array. 


We start by adding a workblock and then adding a day-off block. If $i$ is odd a workblock is added, if $i$ is even a day-off block is added. 
Lets consider case one first, i.e, $i$ is odd: \\
A for-loop takes care of adding every type of work block in the class solution using the
already described $uniqueWorkblocks$ array. The distribution array is modified accordingly and the sequence array is also modified. The pruning of the search tree is done by checking if the type of workblock was even available and
a function called $fullfillsRequirement$. $fullfillsRequirement$ checks the $distribution$ array for days where there is more work done than needed. \\
If $i$ is even a day-off block is added:\\
Again a for-loop takes care of adding every type of day-off block to the solution. Pruning is done by checking if the already modified $distribution$ array has more 
days-off on a day than allowed. This is done with the method $fullfillsRequirementsDayOff$. If a solution suffices the criteria of the requirement matrix, i.e., we have exactly as many
free days as allowed and i is exactly the size of the class solution times two, the next phase is engaged, namely the method $sequences\_shifts\_assignment$ where the workblocks get specific shifts. 

The method $sumTest$ takes care of checking the number of free days in $distribution$ against the required free days (this information can be obtained with the requirement matrix which is part of the instance).
\\
This step is the most time consuming one. We also used a time limit for a class solution to find legal permutations. In this way we were able to solve instances we were not able to solve before that modification.


\begin{lstlisting}[label=detlegal,caption=Determining legal restricted partitions, tabsize=1, keepspaces=true, breaklines=true]
private Solution check_restrictedPartitionsOpt(int i, int[][] uniqueWorkblocks, boolean[] distribution, int distPos,int[] sequence, int sequencePos){
        if (this.solved) {
            return null;
        }
        if (i%2 == 1){
            //add work blocks
            for(int j=0; j<uniqueWorkblocks[0].length; j++ ){
                int workblock =  uniqueWorkblocks[0][j];
                if(workblock+distPos > distribution.length){
                    return null;
                }
                for(int k=distPos; k < distPos+workblock; k++){
                    distribution[k] = false;
                }
                uniqueWorkblocks[1][j]  = uniqueWorkblocks[1][j]-1;
                sequence[sequencePos] = workblock;
                if(uniqueWorkblocks[1][j]>=0 && fullfillsRequirements(tempreqd, scheduleLength, distribution, distPos+workblock)){
                    Solution sol = check_restrictedPartitionsOpt(i+1, uniqueWorkblocks, distribution, distPos + workblock, sequence, sequencePos+1);
                    if (sol != null) {
                      this.solved = true;
                      return sol;
                    }
                }
                //redo for next recursion
                uniqueWorkblocks[1][j]  = uniqueWorkblocks[1][j]+1;
            }
        }
        else{
            //add day-off block
            for (int j = 0; j < max_Dayoff - min_dayoff + 1; j++) {
                int dayOff_block =  min_dayoff + j;
                if(dayOff_block+distPos > distribution.length){
                    return null;
                }
                for(int k=distPos; k < distPos+dayOff_block; k++){
                    distribution[k] = true;
                }
                sequence[sequencePos] = dayOff_block;
                if(i==numberofWorkBlocks*2){
                    if(sumTest(freedays, distribution)){
                        Solution sol =  sequences_shifts_assignment(sequence, 1, 0, new Solution(scheduleLength,nr_Employees), false);
                        if (sol != null) {
                            this.solved = true;
                            return sol;
                        }
                        return null;
                    }
                    //tempreqdoff is a matrix containing the day-off requirements for each day
                } else if(fullfillsRequirementsDayOff(tempreqdoff, scheduleLength, distribution, distPos+dayOff_block)){
                    Solution sol = check_restrictedPartitionsOpt(i+1, uniqueWorkblocks, distribution, distPos+dayOff_block, sequence, sequencePos+1);
                    if (sol != null) {
                        this.solved = true;
                        return sol;
                    }
                }
            }
        }
        return null;
    }
\end{lstlisting}
\section{Generation of permitted shift sequences for each work block}
Once we have determined a legal restricted partition we know the lengths of work blocks occuring in solutions using this restricted partition. For each of these lengths we now generate all possible shift sequences which are consistent with the given constraints. These constraints include the minimal and maximal length of a block of the same shifts and a number of not allowed sequences of shifts.
Because typically the lengths of shifts are not big and the relatively high number of constraints, a simple backtracking algorithm is able to construct all the permitted shift sequences quite fast.\\

The method $generate\_shift\_sequences$ generates all the legal shift sequences of length $wblength$. The array $s$ is used to construct the shift sequencse, $pos$ indicates the position in the array where the algorithm is currently working and $seq$ tells how many subsequent shifts of the same type are currently at the end of the array, which is used to check the maximal length constraints.\\
The for loop iterates through the types of shifts we have to consider, where 0 corresponds to a day shift, 1 to an afternoon shift and 2 to a night shift. First we check if the type of shift we want to add is allowed to follow the previously added type of shift or if we are at the beginning of the array $s$, in which case we definitely are allowed to add the shift. The structure of $shift\_taboo\_lists$ in the instance is used for this check.\\
Then we check if we add the same type of shift as on position $pos - 1$. If we add a different type, we have to consider the minimal length of block constraint and add multiple shifts of the type to fulfill that constraint. Otherwise we add a single shift of the type if this is allowed by the maximal length of block constraint. 
$lpos$ and $lseq$ are local copies of $pos$ and $seq$ and are incremented accordingly.\\
If we have filled the local copy of $s$ completely, we add it to the $shiftSequences$ structure, where it is indexed using $wblength$. Otherwise we simply start a new iteration of the method with $ls$, $lpos$, and $lseq$.

\begin{lstlisting}[label=gensseq,caption=Generation of permitted shift sequences of length $wblength$, tabsize=1, keepspaces=true, breaklines=true]
    private void generate_shift_sequences(int[] s, int pos, int seq, int wblength)
    {
        int last;

        if (pos == 0) {
            last = -1;
        }
        else {
            last = s[pos - 1];
        }


        for (int i = 0; i < this.i.getShifts().length; i++) {
            if (pos == 0 || !this.i.getShift_taboo_lists()[last].contains(i)) {
                int ls[] = s.clone();
                int lpos = pos;
                int lseq = seq;

                if (lpos == 0 || last != i) {
                    //we are at the start of the shseq or adding a different shift than before
                    //add the shift minlength times
                    lseq = 0;
                    int limit = this.i.getShifts()[i].getMinLengthOfBlocks() + lpos;
                    if (limit <= wblength) {
                        for (; lpos < limit; lpos++) {
                            ls[lpos] = i;
                            lseq++;
                        }
                        if (lpos == wblength) {
                            this.shiftSequences.get(wblength).add(ls);
                        }
                        else {
                            generate_shift_sequences(ls, lpos, lseq, wblength);
                        }
                    }
                }
                else {
                    //we add a single shift of the same type as before
                    //check maxlength
                    if (lseq + 1 <= this.i.getShifts()[i].getMaxLengthOfBlocks()) {
                        ls[lpos++] = i;
                        lseq++;
                        if (lpos == wblength) {
                            this.shiftSequences.get(wblength).add(ls);
                        }
                        else {
                            generate_shift_sequences(ls, lpos, lseq, wblength);
                        }
                    }
                }
            }
        }
    }
\end{lstlisting}

\section{Assignment of shift sequences to work blocks}
When checking if a restricted partition is legal in step (ii), we already created a sequence of work blocks and day off blocks which satisfies the requirements matrix if the different types of shifts are disregarded. Now all that is left is to assign shift sequences generated in step (iii) to the work blocks in this sequence. Again we used a backtracking algorithm to do this.
If the instance has not allowed shift sequences which include a day off, these are also considered here.\\

The method for the backtracking algorithm uses following parameters: $sequence$ was generated in step (ii) and contains a sequence of lengths of work and days off blocks alternatingly, $i$ indicates the position in $sequence$ where the algorithm currently stands, $pos$ inidicates the position up to which the partial solution is already constructed, $sol$ is the partial solution, $check\_long\_seqs$ is used to signify if a check of not allowed sequences which include a single day off is required.\\
First, if the end of the sequence is reached, the constructed solution is checked against the temporal requirements and returned if the yare fulfilled. Otherwise we use $i\%2$ to determine if we have to add a work block or a days-off block.\\
If a work block has to be added, we obtain all the legal shift sequences of the length indicated in $sequence$ which were created in step (iii). For each of those shift sequences we create a new partial solution by applying the shift sequence to the current work block and check this partial solution against the temporal requirements. Should $check\_long\_seqs$ be true, we also check against the constraints contained in $shift\_long\_taboo\_lists$ in the instance.\\
If the partial solution $sol$ isn't in conflict with neither the temporal requirements nor eventual not allowed sequences, $i$ is incremented and the next iteration of the algorithm is started.\\
If a days off block has to be added, we simply add the number of days off indicated by $sequence$. If we add a single day, we also set $check\_long\_seqs$ for the next iteration to true. Then the next iteration of the algorithm is started.


\begin{lstlisting}[label=sseqassign,caption=Assignment of shift sequences to work blocks, tabsize=1, keepspaces=true, breaklines=true]
    private Solution sequences_shifts_assignment(int sequence[], int i, int pos, Solution sol, boolean check_long_seqs)
    {
        int wblength = sequence[i-1];

        //if at end
        if (i == sequence.length || wblength == 0) {
            if (sol.check_tempreqs(this.i.getTempreq(), -1)) {
                return sol;
            }
            return null;
        }

        if (i%2 == 1) {
            //add work block
            Set<int[]> sseqs = this.shiftSequences.get(wblength);

            if (sseqs == null) {
                sseqs = new HashSet<int[]>();
                this.shiftSequences.put(wblength, sseqs);

                //generate all sequences of length wblength, honoring the constraints
                generate_shift_sequences(new int[wblength], 0, 0, wblength);
            }

            //try every shift sequence with length wblength
            for (int shiftseq[] : sseqs) {
                boolean taboo = false;

                sol.assign_wblock(pos, shiftseq);

                int tmp_pos = pos + shiftseq.length;

                //check constraint
                if (check_long_seqs) {
                    List<int[]> taboo_list = this.i.getShift_long_taboo_lists();

                    for (int[] tab : taboo_list) {
                        if (sol.is_taboo(pos - 2, tab)) {
                            taboo = true;
                            break;
                        }
                    }
                }

                if (!taboo) {
                    if (sol.check_tempreqs(this.i.getTempreq(), tmp_pos)) {
                        Solution ret = sequences_shifts_assignment(sequence, i + 1, tmp_pos, sol, false);
                        if (ret != null) {
                            return ret;
                        }
                    }
                }
            }

            check_long_seqs = false;
        }
        else {
            //add days off block
            int offseq[] = new int[wblength];
            boolean long_seqs = false;

            if (wblength == 1) {
                long_seqs = true;
            }

            for (int j = 0; j < offseq.length; j++) {
                offseq[j] = -1;
            }

            sol.assign_wblock(pos, offseq);
            pos += wblength;

            Solution ret = sequences_shifts_assignment(sequence, i + 1, pos, sol, long_seqs);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }
\end{lstlisting}
\section{Determination of legal restricted partitions}

\section{Generation of permitted shift sequences for each work block}
Once we have determined a legal restricted partition we know the lengths of work blocks occuring in solutions using this restricted partition. For each of these lengths we now generate all possible shift sequences which are consistent with the given constraints. These constraints include the minimal and maximal length of a block of the same shifts and a number of not allowed sequences of shifts.
Because typically the lengths of shifts are not big and the relatively high number of constraints, a simple backtracking algorithm is able to construct all the permitted shift sequences quite fast.

\section{Assignment of shift sequences to work blocks}
When checking if a restricted partition is legal in step (ii), we already created a sequence of work blocks and day off blocks which satisfies the requirements matrix if the different types of shifts are disregarded. Now all that is left is to assign shift sequences generated in step (iii) to the work blocks in this sequence. Again we used a backtracking algorithm to do this.
If the instance has not allowed shift sequences which include a day off, these are also considered here.

\section{Performance Evaluation}
The performance was evaluated on the following setup:\\ 
Distribution: LinuxMint 15:olivia (x86-64)\\
Processor: Intel Core i5-3570K CPU @ 3.40GHZ x4\\
Memory: 7.8GiB 

\par\bigskip


\begin{tabular}{| l | c | r| }
      \hline                       
      Instance & Time elapsed& Solved\\ 
      \hline                       
      Example1 &7 ms &yes\\
      Example2 &364 ms & yes\\
      Example3 &4632 ms & yes\\
      Example4 &15546 ms & yes\\
      Example5 &10068 ms & yes\\
      Example6 &238 ms & yes\\
      Example7 & - & no\\
      Example8 & 1294472ms & yes\\
      Example9 & - & no\\
      Example10 & 16028 ms & yes\\
      Example11& - & no\\
      Example12& 11549 ms & yes\\
      Example13& 440732 ms & yes\\
      Example14& 6563 ms & yes\\
      Example15& - & no\\
      Example16& - & no\\
      Example17& - & no\\
      Example18& - & no\\
      Example19& - & no\\
      Example20 & - & no\\
      \hline  
    \end{tabular}
\begin{enumerate}
  \item{Example1 in 7ms
      \begin{verbatim}
      NNNN--D
      DAAANN-
      -DDAAAN
      N--DDAA
      ANN--DD
      DD--DDA
      A--NNNN
      --DDAA-
    -AAAA--
    \end{verbatim}
    }
  \item{Example2 in 364ms
      \begin{verbatim}
      AAAA--N
      NNN--DD
      DDDDD--
      NNNNNN-
      -AAAAAA
      ---NNNN
      ----AAA
      A--DDDD
      DDD----
    \end{verbatim}
    }
  \item{Example3 in 4632ms
      \begin{verbatim}
      AANN--A
      ANN--AA
      NN--AAN
      N--AANN
      --AANN-
      --AANN-
      -DDAAAA
      A--DDDD
      DAA--NN
      NN--AAN
      N---DDD
      D--DDDA
      AAA--DD
      DDDDD--
      DDDD---
      AANNN--
      DDDNN--
    \end{verbatim}
    }
  \item{Example4 in 15546ms
      \begin{verbatim}
      AAA----
      NNN----
      DDDNN--
      DDDDDD-
      DDDDDD-
      DDDDDD-
      DDDAAA-
      AAAAAA-
      AAAAAA-
      AAAAAA-
      AAAAAA-
      ---DDD-
      ---DDD-
    \end{verbatim}
    }
  \item{Example5 in 10068ms
      \begin{verbatim}
      AAAA---
      AANNN--
      AAANN--
      DDDDDD-
      DDDDDD-
      DDDDAA-
      -NNNN--
      --AAAAN
      N--AAAN
      NN--DDN
      NNN----
    \end{verbatim}
    }
  \item{Example6 in 238ms
      \begin{verbatim}
      AANNN--
      DDDDDD-
      DDDDDD-
      AAAAAA-
      --AAAAN
      NN----N
      NNNNN--
    \end{verbatim}
    }
   \item{Example8 in 1294472ms
     \begin{verbatim}
      AAA----
      DDDNN--
      DDDAA--
      AANNN--
      DDDDD--
      DDDDD--
      DDDDD--
      AAAAA--
      AAAAA--
      --AAAAN
      NN----N
      NNNN---
      AAAAAA-
      ---DDD-
      ---DDDN
      NNN----
      \end{verbatim}
     }
     

  \item{Example10 in 16028ms
      \begin{verbatim}
      DDAA--D
      DAA--DD
      AA--DDA
      A--DDAA
      --DDAA-
      -DDAA--
      DDAA--D
      DAA--DD
      AA--DDA
      A--DDAA
      --DDAA-
      -DDAA--
      NNNN--N
      NNN--NN
      NN--NNN
      N--NNNN
      --NNNN-
      -NNNN--
      DDDDD--
      DDDDD--
      DDDDD--
      AAAAA--
      AAAAA--
      AAAAA--
      NNNNN--
      NNNNN--
      NNNNN--
    \end{verbatim}
    }
  \item{Example12 in 11549ms
    \begin{verbatim}
    DDDD--D
    DDD--DD
    DDDAA--
    DDDDD--
    DDDDD--
    AAAAA--
    AAAAA--
    DDDDAAA
    --AAAAA
    --DDDDA
    AA--DDD
    DD--DDA
    AAAA--D
    DAAAAA-
    -DDDDD-
    -DDDDDA
    A--DDDA
    A--DDDA
    AAA--DD
    DAAAA--
    \end{verbatim}
    }
  \item{Example13 in 440732ms
    \begin{verbatim}
    DDD--DD
    D--DDN-
    -DDN--D
    DD--AAA
    --DDD--
    DDD--DD
    D--DDD-
    -DDD--D
    DD--DDA
    A--DDDD
    D--DDDA
    A--DDDA
    AAA--DD
    DAAA---
    DDAAA--
    DDDDDD-
    -DDDDD-
    -DDDAA-
    -AAAAA-
    -AAAAA-
    -DDDAAA
    A--DDDA
    AAA--AA
    AAAN---
    \end{verbatim}
    }
  \item{Example14 in 6563ms
    \begin{verbatim}
    DDDD-DD
    DD-DDDD
    -DDDD-D
    DAAAAA-
    AAAAA--
    DDDD-AA
    NNN--AA
    NN--DDA
    A-AAAA-
    AANNN--
    DDDNN--
    DDDDDD-
    DDDDDD-
    \end{verbatim}
    }
\end{enumerate}






\newpage
\bibliography{bibtex}

\end{document}
