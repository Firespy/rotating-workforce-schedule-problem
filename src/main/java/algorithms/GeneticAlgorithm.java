package algorithms;

import model.FitnessClass;
import model.Instance;
import model.SolutionHeuristic;

import java.util.*;
import java.util.logging.FileHandler;

/**
 * Created by svozil on 11.05.14.
 */
public class GeneticAlgorithm {

    Instance instance;
    int daysoffsum;
    int workdayssum;
    SolutionHeuristic f_solution;
    int popsize;
    Random r;

    public GeneticAlgorithm(Instance i) {
        this.instance = i;
    }
    public GeneticAlgorithm() {
    }

    public SolutionHeuristic solve(Instance instance, long runtime){
       int init_population = instance.getNr_employee()*4;
       popsize = init_population;
       HashSet<SolutionHeuristic> population = createSolutions(instance, init_population);
       this.instance = instance;

       r = new Random();
       daysoffsum = (instance.getNr_employee()*instance.getScheduleL())-sumMatrix(instance.getTempreq());
       workdayssum = sumMatrix(instance.getTempreq());
       boolean not_solved = classify(population);

       long loopstart = System.currentTimeMillis();
       //main loop

       while(not_solved){
          if(System.currentTimeMillis()> loopstart+runtime)
              break;
         HashSet<SolutionHeuristic> selected = selection(population);
         HashSet<SolutionHeuristic> recombinated = crossover(selected);
         population = mutation(recombinated);
         not_solved = classify(population);
       // System.out.println("new run: "+ population.size());

       }




        return f_solution;
    }

    private HashSet<SolutionHeuristic> mutation(HashSet<SolutionHeuristic> recombinated) {
        for(SolutionHeuristic sh: recombinated){
            double mutation_prob = r.nextDouble();
            if(mutation_prob < 0.80){
                int randomday = r.nextInt(instance.getScheduleL());
                int randomemployee1 = r.nextInt(instance.getNr_employee());
                int randomemployee2 = r.nextInt(instance.getNr_employee());
                int [][] arr = sh.getArray();

                int random = r.nextInt(5);
                //System.out.println(random);

                int tmpshift = arr[randomday][randomemployee1];
                arr[randomday][randomemployee1] = arr[randomday][randomemployee2];
                arr[randomday][randomemployee2] = tmpshift;

                if(randomday - 1 > instance.getScheduleL() && (random == 3 || random == 0|| random == 4) ){
                    int tmpshift3 = arr[randomday-1][randomemployee1];
                    arr[randomday-1][randomemployee1] = arr[randomday][randomemployee2];
                    arr[randomday-1][randomemployee2] = tmpshift3;
                }

                if(randomday + 2 < instance.getScheduleL() && (random == 3 || random == 1|| random == 4)){
                        int tmpshift2 = arr[randomday+2][randomemployee1];
                        arr[randomday+2][randomemployee1] = arr[randomday][randomemployee2];
                        arr[randomday+2][randomemployee2] = tmpshift2;
                }
                if(randomday + 3 < instance.getScheduleL() && (random == 3 || random == 1|| random == 4 )){
                    int tmpshift2 = arr[randomday+1][randomemployee1];
                    arr[randomday+3][randomemployee1] = arr[randomday][randomemployee2];
                    arr[randomday+3][randomemployee2] = tmpshift2;
                }
            }

        }
        return recombinated;
    }


    private SolutionHeuristic selectRandom(HashSet<SolutionHeuristic> population){
        int size = population.size();
        int item = r.nextInt(size);
        int i = 0;
        SolutionHeuristic solh;
        for(SolutionHeuristic solh2 : population){
            if(i == item){
                solh = solh2;
                return solh;
            }
            i++;
        }
        return  null;

    }
    private SolutionHeuristic elitism(HashSet<SolutionHeuristic> population){
        int i = Integer.MAX_VALUE;
        SolutionHeuristic solh = null;
        for(SolutionHeuristic solh2 : population){
            if(i > solh2.getFitness()){
                solh = solh2;
            }
        }
        return  solh;

    }

    private HashSet<SolutionHeuristic> crossover(HashSet<SolutionHeuristic> population) {

        HashSet<SolutionHeuristic> recombinated = new HashSet<SolutionHeuristic>();
        Iterator<SolutionHeuristic> l = recombinated.iterator();
        while(recombinated.size() < popsize) {
            double crossoverprobability = r.nextDouble();
            if(crossoverprobability<.5) {
                SolutionHeuristic solh_1 = selectRandom(population);
                SolutionHeuristic solh_2 = selectRandom(population);

                for(int i=0; i < instance.getScheduleL(); i++){
                    int rand = r.nextInt();
                    if (rand % 2 == 0) {
                        for(int j =0; j< instance.getNr_employee();j++) {
                            solh_2.getArray()[i][j] = solh_1.getArray()[i][j];
                        }
                    }
                }

               SolutionHeuristic sh = new SolutionHeuristic(instance.getScheduleL(), instance.getNr_employee(),solh_2.getArray());

               recombinated.add(sh);
            }else{
                   recombinated.add(selectRandom(population));
              //  recombinated.add(elitism(population));
                }

            }




        return recombinated;
    }


    //select solutions with minimal fitness
    private HashSet<SolutionHeuristic> selection(HashSet<SolutionHeuristic> population) {
        HashSet<SolutionHeuristic> selected = new HashSet<SolutionHeuristic>();
        TreeMap<Integer, FitnessClass> fitnessClassHashMap = new TreeMap<Integer, FitnessClass>();
      //  System.out.println(population.size());
       // return null;

        //every solution has a class
        for(SolutionHeuristic solh : population){
            if(fitnessClassHashMap.containsKey(solh.getFitness())){
               FitnessClass fc = fitnessClassHashMap.get(solh.getFitness());
               fc.getSolh().add(solh);
            }else {
                FitnessClass fc = new FitnessClass(solh.getFitness());
                fc.getSolh().add(solh);
                fitnessClassHashMap.put(solh.getFitness(), fc);
            }
        }
        //System.out.println(fitnessClassHashMap.descendingKeySet().descendingIterator().next());

        while(selected.size()<popsize*0.40){
//            System.out.println("bla"+ selected.size() + " " + popsize + fitnessClassHashMap.isEmpty()+ fitnessClassHashMap.size());
            double gamble = r.nextDouble();
            for(int i=0; i < fitnessClassHashMap.size(); i++){
                //System.out.println(gamble);
               if(Math.pow(0.5, i+1)< gamble){
                   Iterator<Integer> it = fitnessClassHashMap.navigableKeySet().iterator();
                   Integer l = 0;
                   for(int j=0; j<i+1; j++) {
                       l = it.next();
                  //     System.out.println(l);
                   }

                   selected.add(fitnessClassHashMap.get(l).removeSolh());
                   if(fitnessClassHashMap.get(l).getSolh().isEmpty()){
                      fitnessClassHashMap.remove(l);
                   }
                   break;
               }

            }


        }

        return selected;
    }


    private int classify_singleshift(List<Integer> shift){
        int fitness=0;

        int curType=-1;
        int curTypelength=1;
       // System.out.println("new shift");
       /* for(int i=0; i<shift.size(); i++){
            System.out.print(shift.get(i));
        }
        System.out.print("\n"); */

        for(int i=0; i<shift.size(); i++){
            if(curType != shift.get(i) && curType!=-1){
                if(instance.getShifts()[curType].getMaxLengthOfBlocks() < curTypelength){
                 //   System.out.println("addin maxpenalty: " + (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks()));
                   fitness += (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks());
                }
                if(instance.getShifts()[curType].getMinLengthOfBlocks() > curTypelength){
                  //  System.out.println("addin minpenalty: " +(instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength));
                    fitness += (instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength) ;
                }


                int[] shiftcheckerarry = new int[2];
                shiftcheckerarry[0] = curType;
                shiftcheckerarry[1] = shift.get(i);

               // System.out.println("shift checker array" +shiftcheckerarry[0]+" "+shiftcheckerarry[1]);
                boolean same = true;
                for(int[]ars : instance.getTabooshifts()) {
                    for (int k = 0; k < 2; k++) {
                        if (ars[k] != shiftcheckerarry[k]) {
                            same = false;
                        }
                    }
                    if(same==true){
                       // System.out.println("shift sequence not allowed");
                       fitness++;
                    }

                    same = true;

                }
            curTypelength=1;
            }else if(curType != -1){
                curTypelength++;
            }
            curType = shift.get(i);


        //for(int i=0; i<instance.getShift_taboo_lists().length; i++) {
         //   System.out.print(instance.getShift_taboo_lists()[i]);
       // }
        }
        if(curType==-1){
           curType =  shift.get(1);
        }

        if(instance.getShifts()[curType].getMaxLengthOfBlocks() < curTypelength){
           // System.out.println("addin maxpenalty: " + (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks()));
            fitness += (curTypelength - instance.getShifts()[curType].getMaxLengthOfBlocks());
        }
        if(instance.getShifts()[curType].getMinLengthOfBlocks() > curTypelength){
           // System.out.println("addin minpenalty: " +(instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength));
            fitness += (instance.getShifts()[curType].getMinLengthOfBlocks() - curTypelength) ;
        }
      //  System.out.println("Fitness: " + fitness);


        return fitness;
    }
    //classifies a set of solutions
    public boolean classify(HashSet<SolutionHeuristic> population) {
        int  maxWblocks = instance.getMax_work();
        int  minWblocks = instance.getMin_work();

        int  maxDblocks = instance.getMax_dayoff();
        int  minDblocks = instance.getMin_dayoff();

        for(SolutionHeuristic solh: population){
            List<List<Integer>> shiftset = new ArrayList<List<Integer>>();
            int fitness = 0;

            int[][] sarray = solh.getArray();
            int penaltyWblocks = 0;
            int penaltyDOBlocks = 0;

            int curWblockLength = 0;
            int curDayoffLength = 0;

          int firstint = sarray[0][0];
          boolean skipfirstwork=false;
          boolean skipfirstdayoff=false;

          boolean lWblock = false;
          boolean lDBlock = false;

          List<Integer> curshift= new ArrayList<Integer>();


          if(firstint==-1){
            skipfirstdayoff=true;
            lDBlock=true;
          }else{
            skipfirstwork = true;
            lWblock = true;
          }
          //handle last block
          //check for bad sequence in the last blocks
            int nrw = instance.getNr_employee();
            int nrd = instance.getScheduleL();
          if(instance.isTri_bad_shifts()&&(
             checkthreeshiftsequences(sarray[nrd-2][nrw-1],sarray[nrd-1][nrw-1],sarray[0][0])||
             checkthreeshiftsequences(sarray[nrd-1][nrw-1],sarray[0][0],sarray[1][0])) ){
              fitness++;

          }
          if(skipfirstdayoff==true){

              outerloop:
              for(int j=0; j< instance.getNr_employee(); j++){
              for(int i=0; i< instance.getScheduleL(); i++){
                      if(sarray[i][j] ==-1){
                        curDayoffLength++;
                      }else{
                          break outerloop;
                      }
                  }
              }
              outerloop2:
              for(int j=instance.getNr_employee()-1; 0<=j;j--){
              for(int i=instance.getScheduleL()-1;0<=i; i--){
                      if(sarray[i][j] ==-1){
                          curDayoffLength++;
                      }else{
                          break outerloop2;
                      }

                  }
              }

          }else {
              List<Integer> tmpshift = new ArrayList<Integer>();
              outerloop4:
              for(int j=instance.getNr_employee()-1; 0<=j;j--){
                  for(int i=instance.getScheduleL()-1;0<=i; i--){
                      if(sarray[i][j] !=-1){
                          tmpshift.add(sarray[i][j]);

//                          curshift[curWblockLength] = sarray[i][j];
                          curWblockLength++;
                      }else{
                          break outerloop4;
                      }

                  }
              }
              for(int i= tmpshift.size()-1; i>=0; i--){
                  curshift.add(tmpshift.get(i));
              }

              outerloop3:
              for(int j=0; j< instance.getNr_employee(); j++){
                 for(int i=0; i< instance.getScheduleL(); i++){
                      if(sarray[i][j] !=-1){
                          curshift.add(sarray[i][j]);
                          curWblockLength++;

                      }else{
                          break outerloop3;
                      }
                  }
              }


          }
          //handle everything but the last block
            //System.out.println();
            //System.out.println("dayoff " + curDayoffLength);
            //System.out.println("wblock " + curWblockLength);
            //System.out.print(solh);
         for(int j=0; j< instance.getNr_employee(); j++){
          for(int i=0; i< instance.getScheduleL(); i++){

                    if(skipfirstdayoff || skipfirstwork){
                        if(skipfirstwork && sarray[i][j] == -1){
                            skipfirstwork = false;
                        }else if(skipfirstwork&& sarray[i][j] != -1){
                            continue;
                        }
                        if(skipfirstdayoff && sarray[i][j] == -1){
                            continue;
                        }else if(skipfirstdayoff && sarray[i][j] != -1){
                            skipfirstdayoff=false;
                        }

                    }

                    if(sarray[i][j]==-1){
                        if(lWblock){
                        if(curWblockLength > maxWblocks )
                        {
                            penaltyWblocks += curWblockLength-maxWblocks;
                           // System.out.println("putting max penalty: "+ (curWblockLength-maxWblocks));
                        }
                        if(curWblockLength < minWblocks)
                        {
                            penaltyWblocks += minWblocks - curWblockLength;
                            //System.out.println("putting min penalty: "+ (minWblocks - curWblockLength) );
                        }
                        shiftset.add(curshift);
                            if(instance.isTri_bad_shifts()) {
                                if (i + 1 < instance.getScheduleL()) {
                                    int nextint = sarray[i + 1][j];
                                    if (nextint != -1) {
                                        //System.out.println(solh);
                                        if (checkthreeshiftsequences(curshift.get(curWblockLength - 1),
                                                sarray[i][j],
                                                nextint)== true) {
                                            fitness++;
                                        }
                                    }
                                } else if (j + 1 < instance.getNr_employee()) {
                                    int nextint = sarray[0][j + 1];
                                    if (nextint != -1) {
                                       // System.out.println(solh+" " +curshift.get(curWblockLength - 1) + " " + sarray[i][j] + " "+
                                         //       nextint );
                                        if (checkthreeshiftsequences(curshift.get(curWblockLength - 1),
                                                sarray[i][j],
                                                nextint) == true) {
                                            fitness++;
                                        }
                                    }


                                }
                            }
                            curshift = new ArrayList<Integer>();// new int[instance.getNr_employee() * instance.getScheduleL()];

                            curWblockLength=0;
                            lWblock=false;
                        }
                        curDayoffLength++;
                        lDBlock = true;

                    }
                    if(sarray[i][j]!=-1){
                        if(lDBlock){
                        if(curDayoffLength > maxDblocks){
                            penaltyDOBlocks += curDayoffLength - maxDblocks;
                        }
                        if(curDayoffLength < minDblocks){
                            penaltyDOBlocks += minDblocks - curDayoffLength;
                        }
                        curDayoffLength = 0;
                        lDBlock = false;
                        }

                        curshift.add(sarray[i][j]);
                        curWblockLength++;
                        lWblock = true;

                    }

                }


            }
            //handle final block if first and last block a different
            int lastint = sarray[instance.getScheduleL()-1][instance.getNr_employee()-1];

            if(firstint == -1 && lastint !=-1 || firstint != -1 && lastint ==-1){
                if(lWblock){
                    if(curWblockLength > maxWblocks )
                    {
                        penaltyWblocks += curWblockLength-maxWblocks;
                    }
                    if(curWblockLength < minWblocks)
                    {
                        penaltyWblocks += minWblocks - curWblockLength;
                    }
                }
                if(lDBlock){
                    if(curDayoffLength > maxDblocks){
                        penaltyDOBlocks += curDayoffLength - maxDblocks;
                        //System.out.println("putting max penalty: "+ (curDayoffLength - maxDblocks));
                    }
                    if(curDayoffLength < minDblocks){
                        penaltyDOBlocks += minDblocks - curDayoffLength;
                        //System.out.println("putting min penalty: "+ (minDblocks - curDayoffLength) );
                    }
                    if(!curshift.isEmpty())
                    shiftset.add(curshift);
                }

            }




            //System.out.println(penaltyDOBlocks+" "+penaltyWblocks);
           int sum_shifts_fitness = 0;
            for(List<Integer> shift : shiftset){
                  sum_shifts_fitness+= classify_singleshift(shift);
              //      System.out.print(shift.get(i));
             //   System.out.print("\n");
            }

            fitness += penaltyDOBlocks + penaltyWblocks + sum_shifts_fitness;
           // System.out.println(fitness);
            if(fitness==0) {
                f_solution = solh;
                System.out.println("found solution");
                return false;
            }
            solh.setFitness(fitness);
          //  System.out.println(solh.toString() + solh.getFitness());
        }
            return true;
    }

    //todo fix it
    private boolean checkthreeshiftsequences(int lastint, Integer curint, int nextint) {
        for(int[] tb : instance.getTabooshifts()){
            if(tb.length > 2){
                if(tb[0] == lastint && tb[1] == curint && tb[2]== nextint){
                    return true;
                }
            }
        }
        return false;
    }

    private HashSet<SolutionHeuristic> createSolutions(Instance instance, int init_population) {
        HashSet<SolutionHeuristic> population = new HashSet<SolutionHeuristic>();
        GreedyRandom gr = new GreedyRandom();
        for(int i=0; i<init_population; i++){
            population.add(gr.createRandom(instance));
        }
        return population;
    }

    private int sumMatrix(int[][] a){
        int sum = 0;
        for(int j = 0; j < instance.getNr_shifts(); j++){
            for(int i = 0; i < instance.getScheduleL(); i++){
                sum+= a[j][i];
            }
        }

        return sum;
    }

}
