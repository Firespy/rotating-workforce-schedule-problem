package util;

import model.Instance;
import model.Shift;
import sun.misc.Regexp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by svozil on 4/3/14.
 */
public class Parser {

    public Instance parseTextInstance(String path){
        Instance instance = new Instance();

        Scanner sc = null;
        try {
            sc = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int sum = 0;

        while(sc.hasNext()){
            String str = sc.nextLine();
            if(str.contains("#Length of the schedule")){
                str = sc.nextLine();
                instance.setScheduleL(Integer.valueOf(str));
            }

            if(str.contains("#Number of Employees")){
                str = sc.nextLine();
                instance.setNr_employee(Integer.valueOf(str));

            }

            if(str.contains("##Number of Shifts")||str.contains("#Number of Shifts")){
                str = sc.nextLine();
                instance.setNr_shifts(Integer.valueOf(str));

            }

            if(str.contains("# Temporal Requirements Matrix")){
                int reqMatr[][] = new int[instance.getNr_shifts()][instance.getScheduleL()];
                int counter = 0;
                str = sc.nextLine();
                while(counter < instance.getNr_shifts()){
                    String numbers [] = str.split(" ");
                    for(int i= 0;i<instance.getScheduleL(); i++ ){
                        reqMatr[counter][i] = Integer.valueOf(numbers[i]);
                    }
                    counter++;

                    str = sc.nextLine();

                }
                /*for(int i = 0; i<3; i++){
                   for(int j=0; j<7; j++) {
                    System.out.print(reqMatr[i][j]);
                   }
                    System.out.println("\n");
                }*/
                instance.setTempreq(reqMatr);
            }

            if(str.contains("#ShiftName, Start, Length, Name, MinlengthOfBlocks, MaxLengthOfBlocks")){
                int counter = 0;
                Shift[] shifts = new Shift[instance.getNr_shifts()];
                str = sc.nextLine();

                while(counter < instance.getNr_shifts()){
                    String k[] = str.split("  | ");
                    Shift shift= new Shift();
                    shift.setName(k[0]);
                    shift.setMinLengthOfBlocks(Integer.valueOf(k[3]));
                    shift.setMaxLengthOfBlocks(Integer.valueOf(k[4]));
                    str = sc.nextLine();
                    shifts[counter] = shift;
                    counter++;
                }

                instance.setShifts(shifts);

            }

            if(str.contains("# Minimum and maximum length of days-off blocks")){
                str = sc.nextLine();
                String k[] = str.split(" ");
                instance.setMin_dayoff(Integer.valueOf(k[0]));
                instance.setMax_dayoff(Integer.valueOf(k[1]));

            }

            if(str.contains("# Minimum and maximum length of work blocks")){
                str = sc.nextLine();
                String k[] = str.split(" ");
                instance.setMin_work(Integer.valueOf(k[0]));
                instance.setMax_work(Integer.valueOf(k[1]));

            }

            if(str.contains("# Number of not allowed shift sequences: NrSequencesOfLength2, NrSequencesOfLength3:")){

                str = sc.nextLine();
                String k[] = str.split(" ");
                sum = Integer.valueOf(k[0]) + Integer.valueOf(k[1]);

            }

            if(str.contains("# Not allowed shift sequences")){
                HashSet<String> nAShifts = new HashSet<String>();

                int counter = 0;
                while(counter < sum){
                    str = sc.nextLine();
                    String str2 = str.replace(" ","");
                    nAShifts.add(str2);
                    counter++;

                }
                instance.setShifts_notAllowed(nAShifts);

            }

        }



        return instance;
    }

}
